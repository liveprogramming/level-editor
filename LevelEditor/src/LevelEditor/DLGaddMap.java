package LevelEditor;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class DLGaddMap extends JDialog {

    private JSpinner spWidth, spHeight, spSize;
    private JComboBox<GameObject> cbGround;
    private boolean ready = false;

    public DLGaddMap(JFrame owner, ArrayList<GameObject> possibleObjects) {
        super(owner, "Create new map", true);

        JButton btCreate = new JButton();
        JButton btCancel = new JButton();
        spWidth = new JSpinner(new SpinnerNumberModel(30, 10, 500, 1));
        spHeight = new JSpinner(new SpinnerNumberModel(30, 10, 500, 1));
        spSize = new JSpinner(new SpinnerNumberModel(25, 10, 500, 1));
        cbGround = new JComboBox<>(possibleObjects.toArray(new GameObject[0]));

        btCreate.setAction(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ready = true;
                DLGaddMap.this.setVisible(false);

            }
        });
        btCancel.setAction(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DLGaddMap.this.setVisible(false);
            }
        });

        btCreate.setText("Create");
        btCancel.setText("Cancel");

        this.setSize(200, 200);
        this.setLayout(new GridLayout(9, 1, 5, 5));
        this.setLocationRelativeTo(null);
        this.add(new JLabel("Breite"));
        this.add(spWidth);
        this.add(new JLabel("Höhe"));
        this.add(spHeight);
        this.add(new JLabel("Tile Size"));
        this.add(spSize);
        this.add(cbGround);
        this.add(btCreate);
        this.add(btCancel);
        this.pack();
    }

    public boolean isReady() {
        return ready;
    }

    public int getLevelWidth() {
        try {
            spWidth.commitEdit();
        } catch (ParseException ex) {
            JOptionPane.showInputDialog(this, ex);
            spWidth.setValue(30);
        }
        return (Integer) spWidth.getValue();
    }

    public int getLevelHeight() {
        try {
            spHeight.commitEdit();
        } catch (ParseException ex) {
            JOptionPane.showInputDialog(this, ex);
            spHeight.setValue(30);
        }
        return (Integer) spHeight.getValue();
    }

    public int getTileSize() {
        try {
            spSize.commitEdit();
        } catch (ParseException ex) {
            JOptionPane.showInputDialog(this, ex);
            spSize.setValue(30);
        }
        return (Integer) spSize.getValue();
    }

    public GameObject getGround() {
        return (GameObject) cbGround.getSelectedItem();
    }

}
