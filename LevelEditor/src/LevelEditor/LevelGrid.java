package LevelEditor;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import javax.swing.JPanel;

public class LevelGrid extends JPanel {

    private int width;
    private int height;
    private int currentLayer;
    private int size;
    ArrayList<Layer> layers = new ArrayList<>();
    
    public LevelGrid(){
        
    }

    public int getCurrentLayer() {
        return currentLayer;
    }

    public void setCurrentLayer(int currentLayer) {
        this.currentLayer = currentLayer;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public double getTranslationX() {
        return translationX;
    }

    public void setTranslationX(double translationX) {
        this.translationX = translationX;
    }

    public double getTranslationY() {
        return translationY;
    }

    public void setTranslationY(double translationY) {
        this.translationY = translationY;
    }

    private double scale = 1;
    private double translationX = 0;
    private double translationY = 0;

    public LevelGrid(int width, int height, ItemGrid plItems) {
        this.width = width;
        this.height = height;
        MouseAdapter ma = new MouseAdapter() {
            private int x0 = 0;
            private int y0 = 0;

            @Override
            public void mouseMoved(MouseEvent e) {
                x0 = e.getX();
                y0 = e.getY();
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                translationX = translationX + e.getX() - x0;
                translationY = translationY + e.getY() - y0;
                x0 = e.getX();
                y0 = e.getY();
                LevelGrid.this.repaint();
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (layers.isEmpty()) {
                    return;
                }

                int x = (int) ((e.getX() - translationX) / (25 * scale));
                int y = (int) ((e.getY() - translationY) / (25 * scale));
                if (x < 0 || y < 0) {
                    return;
                }

                Layer layer = layers.get(currentLayer);

                if (e.getButton() == 1) {
                    layer.set(x, y, plItems.getCurrentGO());
                } else if (e.getButton() == 3) {
                    if (currentLayer != 0) {
                        layer.set(x, y, null);
                    }
                }

                LevelGrid.this.repaint();
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
                    if (e.getUnitsToScroll() < 0) {
                        scale = scale * 1.1;
                    } else {
                        scale = scale * 0.9;
                    }
                    LevelGrid.this.repaint();
                }
            }
        };
        this.addMouseListener(ma);
        this.addMouseMotionListener(ma);
        this.addMouseWheelListener(ma);
    }

    public ArrayList<Layer> getLayers() {
        return layers;
    }

    public void setLayers(ArrayList<Layer> layers) {
        this.layers = layers;
        this.currentLayer = 0;
        this.repaint();
    }

<<<<<<< HEAD
    public void resetMap(int width, int height, GameObject go, String name, boolean b, int size) {
=======
    public void resetMap(int width, int height, GameObject go, String name) {
>>>>>>> 209c0c1e572cb0d0d52fa6091f01837f1837bb13
        this.width = width;
        this.size = size;
        this.height = height;
        layers.clear();
        currentLayer = 0;
        Layer layer = new Layer(width, height, name);
        layers.add(layer);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                layer.set(x, y, go);
            }
        }
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (!layers.isEmpty()) {
            Graphics2D g2d = (Graphics2D) g;

            g2d.translate(translationX, translationY);
            g2d.scale(scale, scale);

            for (Layer layer : layers) {
                for (int x = 0; x < layer.getWidth(); x++) {
                    for (int y = 0; y < layer.getHeight(); y++) {
                        GameObject go = layer.get(x, y);

                        if (go != null) {
                            g2d.drawImage(go.getLook(), x * size, y * size, size, size, null);
                        }
                    }
                }
            }
        }
    }

    public void addLayer(String name) {
        Layer layer = new Layer(this.width, this.height, name);
        layers.add(layer);
    }
}
