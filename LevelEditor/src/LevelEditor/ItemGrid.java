package LevelEditor;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

public class ItemGrid extends JPanel {

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }

    private int curIndex = 0;
    private ArrayList<GameObject> gameObjects;
    private ArrayList<JToggleButton> toggleButtons;

    public ItemGrid(ArrayList<GameObject> gameObjects) {
        this.gameObjects = gameObjects;
        int rows = 1 + gameObjects.size() / 2;
        toggleButtons = new ArrayList<>(gameObjects.size());
        GridLayout gl = new GridLayout(rows, 2, 25, 25);
        
        this.setLayout(gl);

        for (int i = 0; i < gameObjects.size(); i++) {
            GameObject go = gameObjects.get(i);
            ImageIcon icon = new ImageIcon(go.getLook());
            JToggleButton tb = new JToggleButton(go.toString(), icon, true);
            tb.setSelected(false);
            tb.setHorizontalTextPosition(JButton.CENTER);
            tb.setVerticalTextPosition(JButton.BOTTOM);
            toggleButtons.add(tb);

            final int asdf = i;

            tb.addActionListener(e -> {
                for (int j = 0; j < toggleButtons.size(); j++) {
                    JToggleButton t = toggleButtons.get(j);
                    t.setSelected(false);
                }
                tb.setSelected(true);
                curIndex = asdf;
            });

            this.add(tb);
        }
        toggleButtons.get(0).setSelected(true);
    }

    public GameObject getCurrentGO() {
        return gameObjects.get(curIndex);
    }

}
