package LevelEditor;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GameObject {

    private ImageFactory imageFactory = ImageFactory.getImageFactory();
//    @XmlElement
    private String imgName;
//    @XmlElement
    private String displayName;
    @XmlElement
    private int id;

    public String getImgName() {
        return imgName;
    }

    public int getID() {
        return id;
    }

    public GameObject(int id, String imgName, String displayName) {
        this.imgName = imgName;
        this.id = id;
        this.displayName = displayName;
    }

    public BufferedImage getLook() {
        return imageFactory.getLooks(id);
    }

    @Override
    public String toString() {
        return displayName;
    }

    public GameObject() {
        id = -1;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    
}
