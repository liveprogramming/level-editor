package LevelEditor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Layer {

    @XmlElement
    private GameObject[][] gameObjects;

    public void setGameObjects(GameObject[][] gameObjects) {
        this.gameObjects = gameObjects;
    }

    @XmlElement
    private String name;

    public Layer(int width, int height, String name) {
        this.gameObjects = new GameObject[width][height];
        for(int x = 0; x < gameObjects.length; x++){
            for(int y = 0; y<gameObjects[x].length; y++){
                gameObjects[x][y] = new GameObject(-1, "", "");
            }
        }
        this.name = name;
    }

    public GameObject[][] getGameObjects() {
        return gameObjects;
    }

    public String getName() {
        return name;
    }

    public void set(int x, int y, GameObject currentGO) {
        gameObjects[x][y] = currentGO;
    }

    public int getWidth() {
        return gameObjects.length;
    }

    public int getHeight() {
        return gameObjects[0].length;
    }

    public GameObject get(int x, int y) {
        return gameObjects[x][y];
    }

    @Override
    public String toString() {
        return "Layer{" + "gameObjects=" + gameObjects + ", name=" + name + '}';
    }
}
