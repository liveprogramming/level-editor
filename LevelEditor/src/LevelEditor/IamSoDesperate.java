
package LevelEditor;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IamSoDesperate {
    @XmlElement
    ArrayList<Layer> layers = new ArrayList<>();
    
}
