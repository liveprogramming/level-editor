package LevelEditor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Editor extends JFrame {

    private LevelGrid grid;

    public Editor() {
        JPanel controls = new JPanel();
        JComboBox<String> cbMapLayers = new JComboBox<>();
        cbMapLayers.setEnabled(false);

        JButton btNewMap = new JButton();
        JButton btSave = new JButton();
        JButton btLoad = new JButton();
        JButton btNewLayer = new JButton("New Layer");
        btNewLayer.setEnabled(false);
        this.setSize(800, 500);
        btSave.setEnabled(false);

        ArrayList<GameObject> items = loadGameObjects();

        ItemGrid plItems = new ItemGrid(items);
        float scale = 0.75f;
        int width = (int) (this.getWidth() * (1 - scale));
        plItems.setPreferredSize(new Dimension(width, this.getHeight()));

        int widthPlGrid = (int) (this.getWidth() * scale);
        grid = new LevelGrid(widthPlGrid, this.getHeight(), plItems);

        cbMapLayers.addActionListener((ActionEvent e) -> {
            grid.setCurrentLayer(cbMapLayers.getSelectedIndex());
        });
        
        btNewLayer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int itemCount = cbMapLayers.getItemCount();
                    String name = JOptionPane.showInputDialog(Editor.this, "Gib bitte den Namen der Layer ein:");
                    if (name == null) {
                        return;
                    }
                    grid.addLayer(name);
                    cbMapLayers.addItem("" + (itemCount+1));
                    cbMapLayers.setSelectedIndex(cbMapLayers.getItemCount() - 1);
            }
        });

        btNewMap.addActionListener(e -> {
<<<<<<< HEAD
            DLGaddMap dlg = new DLGaddMap(Editor.this, this.getPossibleBackgrounds(items, "rock0"));
=======
            DLGaddMap dlg = new DLGaddMap(Editor.this, this.getPossibleBackgrounds(items, "grass0"));
>>>>>>> 209c0c1e572cb0d0d52fa6091f01837f1837bb13
            dlg.setVisible(true);

            if (dlg.isReady()) {
                cbMapLayers.removeAllItems();
                String name = JOptionPane.showInputDialog(this, "Gib bitte den Namen der Layer ein:");
                if (name == null) {
                    return;
                }
<<<<<<< HEAD
                int res = JOptionPane.showConfirmDialog(this, "Sollen auf dieser Layer collisons stattfinden können?", "Collisions", JOptionPane.YES_NO_OPTION);
                if (res == JOptionPane.CLOSED_OPTION) {
                    return;
                }
                boolean b = res == JOptionPane.YES_OPTION ? true : false;
                grid.resetMap(dlg.getLevelWidth(), dlg.getLevelHeight(), dlg.getGround(), name, b, dlg.getTileSize());
=======
                grid.resetMap(dlg.getLevelWidth(), dlg.getLevelHeight(), dlg.getGround(), name);
>>>>>>> 209c0c1e572cb0d0d52fa6091f01837f1837bb13
                cbMapLayers.addItem("1");
                cbMapLayers.setEnabled(true);
                btSave.setEnabled(true);
                btNewLayer.setEnabled(true);
            }
        });

        btSave.addActionListener((ActionEvent e) -> {
            JFileChooser fc = new JFileChooser();
            fc.showSaveDialog(Editor.this);

            File f = fc.getSelectedFile();

            if (f != null) {
                DAL dal = new DAL(items);
                try {
                    dal.save(grid.getLayers(), f);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        btLoad.addActionListener(e -> {
            JFileChooser fc = new JFileChooser();
            fc.showOpenDialog(Editor.this);
            File f = fc.getSelectedFile();

            if (f != null) {
                try {
                    DAL dal = new DAL(items);
                    ArrayList<Layer> layers = dal.load(f);
                    grid.setLayers(layers);
                    btSave.setEnabled(true);
                    cbMapLayers.setEnabled(true);
                    cbMapLayers.removeAllItems();

                    for (int i = 0; i < layers.size(); i++) {
                        cbMapLayers.addItem("" + (i + 1));
                    }
                    btNewLayer.setEnabled(true);
                    grid.repaint();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(this, ex);
                    ex.printStackTrace();
                }
            }
        });

        btNewMap.setText("New Map");
        btSave.setText("Save");
        btLoad.setText("Load");

        controls.setSize(controls.getWidth(), 20);
        controls.setLayout(new GridLayout(1, 6, 10, 10));
        controls.add(btNewMap);
        controls.add(cbMapLayers);
        controls.add(btNewLayer);
        controls.add(btSave);
        controls.add(btLoad);

        this.setLayout(new BorderLayout(5, 5));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.add(controls, BorderLayout.NORTH);
        this.add(grid, BorderLayout.CENTER);
        this.add(plItems, BorderLayout.EAST);

    }

    private ArrayList<GameObject> loadGameObjects() {
        ArrayList<GameObject> list = new ArrayList<>();
        int id = 0;
        File pack = new File("./build/classes/GameObjects");
        File[] listFiles = pack.listFiles();
        Arrays.sort(listFiles, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });

        for (File listFile : listFiles) {
            String name = listFile.getName();
            name = name.substring(0, name.lastIndexOf("."));

            String displayName = getDisplayName(name);
            GameObject go = new GameObject(id, name, displayName);
            list.add(go);
            id++;
        }
        return list;
    }

    private String getDisplayName(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1, line.length() - 1);
    }

    public static void main(String[] args) {
        Editor builder = new Editor();
        builder.setVisible(true);
    }

    private ArrayList<GameObject> getPossibleBackgrounds(ArrayList<GameObject> items, String... imgNames) {
        ArrayList<GameObject> possible = new ArrayList<>();

        checkItems:
        for (int i = 0; i < items.size(); i++) {
            GameObject item = items.get(i);
            for (String name : imgNames) {
                if (item.getImgName().equalsIgnoreCase(name)) {
                    possible.add(item);
                    continue checkItems;
                }
            }
        }
        
        return possible;
    }
}
