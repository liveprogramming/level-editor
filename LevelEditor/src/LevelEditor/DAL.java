package LevelEditor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.TreeMap;
import javax.xml.bind.JAXB;

public class DAL {

    private static final TreeMap<Integer, GameObject> items = new TreeMap<>();

    public DAL(ArrayList<GameObject> list) {
        for (GameObject item : list) {
            items.put(item.getID(), item);
        }
    }

    public void save(ArrayList<Layer> layers, File out) throws FileNotFoundException {
        IamSoDesperate iasd = new IamSoDesperate();
        iasd.layers = layers;
        JsonArray root = new JsonArray();
        for (Layer layer : layers) {
            JsonArray tmp = new JsonArray();
            for (GameObject[] x : layer.getGameObjects()) {
                JsonArray tmp2 = new JsonArray();
                for (GameObject y : x) {
                    tmp2.add(y.getID());
                }
                tmp.add(tmp2);
            }
            root.add(tmp);
        }

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String result = gson.toJson(root);
        PrintWriter pw = new PrintWriter(out);
        pw.print(result);
        pw.close();
    }

    public ArrayList<Layer> load(File f) throws FileNotFoundException {
        Gson gson = new Gson();
        
        JsonArray root = gson.fromJson(new FileReader(f), JsonArray.class);
        
        ArrayList<Layer> tmpList = new ArrayList();
        
        for(JsonElement t : root)
        {
            JsonArray tmp = t.getAsJsonArray();
            GameObject[][] gameObject = new GameObject[tmp.size()][tmp.get(0).getAsJsonArray().size()];
            for(int i = 0; i< tmp.size(); i++)
            {
                JsonArray tmp2 = tmp.get(i).getAsJsonArray();
                for(int j = 0; j<tmp2.size(); j++) {
                    GameObject a = new GameObject();
                    a.setId(tmp2.get(j).getAsInt());
                    gameObject[i][j] = a;
                }
            }
            
            Layer l = new Layer(gameObject.length, gameObject[0].length, "");
            l.setGameObjects(gameObject);
            tmpList.add(l);
        }
        
        return tmpList;
    }
}
