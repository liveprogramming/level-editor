package LevelEditor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class ImageFactory {

    private static ImageFactory imgF;
    private HashMap<Integer, BufferedImage> map = new HashMap<>();

    public static ImageFactory getImageFactory() {
        if(imgF == null){
            imgF = new ImageFactory();
        }
        return imgF;
    }

    private ImageFactory(){
        
        File pack = new File("./build/classes/GameObjects");
        File[] listFiles = pack.listFiles();
        Arrays.sort(listFiles, new Comparator<File>(){
            @Override
            public int compare(File o1, File o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
            
        });
        int id = 0;
        for (File listFile : listFiles) {
            try {
                String name = listFile.getName();
                name = name.substring(0, name.lastIndexOf("."));
                map.put(id, ImageIO.read(listFile));
                id++;
            } catch (IOException ex) {
                Logger.getLogger(ImageFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public BufferedImage getLooks(int value){
        return map.get(value);
    }
}
